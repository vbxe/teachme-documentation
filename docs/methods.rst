######
Methods
######

*****
Login
*****
Authenticate the user with the system and obtain the unique_identifer (user token)

**Request**
+--------+--------------------------------+
| Method | URL                            |
+========+================================+
| POST   | /teachme/api/                  |
+--------+--------------------------------+

+--------+---------------+---------------+
| Type   | Params        | Values        |
+========+===============+===============+
| HEAD   | api_key       | string        |
+--------+---------------+---------------+
| POST   | username      | string        |
+--------+---------------+---------------+
| POST   | password      | string        |
+--------+---------------+---------------+
