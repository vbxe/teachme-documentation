.. Teach.Me API documentation master file, created by
   sphinx-quickstart on Sun May 29 21:00:53 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Teach.Me API Documentation
========================================

Contents:

.. toctree::
   :maxdepth: 2
   
   methods



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

